package com.seymourapplications.libredictionary;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;

import com.seymourapplications.dictionary.Dictionary;

import java.io.ObjectInputStream;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static BottomNavigationBar bottomNavigationBar;
    private ViewPager mViewPager;
    private static Handler UIHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottom_navigation_bar);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        if(mViewPager != null) {
            mViewPager.setAdapter(mSectionsPagerAdapter);

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }
                @Override
                public void onPageSelected(int position) {
                    if(bottomNavigationBar.getCurrentSelectedPosition() != position) {
                        bottomNavigationBar.selectTab(position);
                    }
                }
                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }

        if(bottomNavigationBar != null) {
            bottomNavigationBar
                    .addItem(new BottomNavigationItem(android.R.drawable.ic_search_category_default, "Dictionary"))
                    .addItem(new BottomNavigationItem(android.R.drawable.ic_menu_share, "Word"))
                    .addItem(new BottomNavigationItem(android.R.drawable.ic_menu_camera, "Recognition"))
                    .addItem(new BottomNavigationItem(android.R.drawable.ic_menu_sort_by_size, "Flashcards"))
                    .initialise();

            bottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener(){
                @Override
                public void onTabSelected(int position) {
                    mViewPager.setCurrentItem(position);
                }
                @Override
                public void onTabUnselected(int position) {
                }
                @Override
                public void onTabReselected(int position) {
                }
            });

            UIHandler = new Handler(Looper.getMainLooper());
        }

        final EditText search = (EditText) findViewById(R.id.search);
        if(search != null) {
            search.setOnTouchListener(new View.OnTouchListener() {
                final int DRAWABLE_RIGHT = 2;

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        int leftEdgeOfRightDrawable = search.getRight() - search.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                        if (event.getRawX() >= leftEdgeOfRightDrawable) {
                            search.setText("");
                            return true;
                        }
                    }
                    return false;
                }
            });
            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(bottomNavigationBar != null) {
                        bottomNavigationBar.selectTab(0);
                    }
                }
                @Override
                public void afterTextChanged(Editable s) {
                }
            });
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(drawer != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
        }

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if(navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if(drawer != null) {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dict) {
            bottomNavigationBar.selectTab(0);
        } else if (id == R.id.nav_recog) {
            bottomNavigationBar.selectTab(1);
        } else if (id == R.id.nav_flash) {
            bottomNavigationBar.selectTab(2);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if(drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";
        private View rootView;

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            switch(getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1:
                    rootView = inflater.inflate(R.layout.fragment_dict, container, false);

                    final View wordView = inflater.inflate(R.layout.fragment_word, container, false);

                    final ListView words_list = (ListView) rootView.findViewById(R.id.words_list);
                    words_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Dictionary.Word word = (Dictionary.Word) parent.getItemAtPosition(position);

                            final TextView word_txt = (TextView) wordView.findViewById(R.id.word);
                            final TextView pron_txt = (TextView) wordView.findViewById(R.id.pron_txt);
                            final LinearLayout definitions = (LinearLayout) wordView.findViewById(R.id.definitions);

                            word_txt.setText(word.Word);

                            bottomNavigationBar.selectTab(1);
                        }
                    });

                    Spinner lang_spin = (Spinner) rootView.findViewById(R.id.lang_spin);
                    lang_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {
                            if(position != 0) {
                                final ProgressDialog progress = ProgressDialog.show(getContext(), "Please Wait", "Loading dictionary...", true);
                                progress.setCancelable(false);
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Dictionary dictionary = null;
                                        switch (position) {
                                            case 1:
                                                try {
                                                    ObjectInputStream ois = new ObjectInputStream(getContext().getResources().openRawResource(R.raw.cedict_raw));
                                                    dictionary = (Dictionary) ois.readObject();
                                                } catch(Exception e) {
                                                    e.printStackTrace();
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                        final WordListArrayAdapter adapter = new WordListArrayAdapter(getContext(), dictionary.getWordsArray());

                                        UIHandler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                words_list.setAdapter(adapter);
                                                progress.dismiss();
                                            }
                                        });
                                    }
                                }).start();
                            } else {
                                words_list.setAdapter(null);
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                        }
                    });
                    break;
                case 2:
                    rootView = inflater.inflate(R.layout.fragment_word, container, false);
                    break;
                case 3:
                    rootView = inflater.inflate(R.layout.fragment_char_recognition, container, false);
                    break;
                default:
                    rootView = inflater.inflate(R.layout.fragment_error, container, false);
                    break;
            }

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Dictionary";
                case 1:
                    return "Word";
                case 2:
                    return "Character Recognition";
                case 3:
                    return "Flashcards";
            }
            return null;
        }
    }

    public static class WordListArrayAdapter extends ArrayAdapter<Dictionary.Word> {
        private final Context ctx;
        private final Dictionary.Word[] words;

        public WordListArrayAdapter(Context ctx, Dictionary.Word[] words) {
            super(ctx, -1, words);
            this.ctx = ctx;
            this.words = words;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.layout_row, parent, false);

            ImageView img = (ImageView) rowView.findViewById(R.id.locale_img);
            switch(words[position].Lang) {
                case ZH:
                    img.setImageDrawable(ctx.getResources().getDrawable(R.drawable.zh));
                    break;
            }

            TextView word_txt = (TextView) rowView.findViewById(R.id.word_txt);
            word_txt.setText(words[position].Word);

            return rowView;
        }
    }
}
