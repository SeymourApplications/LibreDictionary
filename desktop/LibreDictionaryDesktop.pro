#-------------------------------------------------
#
# Project created by QtCreator 2016-06-12T21:30:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LibreDictionaryDesktop
TEMPLATE = app


SOURCES += main.cpp\
        libredictionary.cpp

HEADERS  += libredictionary.h

FORMS    += libredictionary.ui
