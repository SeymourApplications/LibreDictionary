#ifndef LIBREDICTIONARY_H
#define LIBREDICTIONARY_H

#include <QMainWindow>

namespace Ui {
class LibreDictionary;
}

class LibreDictionary : public QMainWindow
{
    Q_OBJECT

public:
    explicit LibreDictionary(QWidget *parent = 0);
    ~LibreDictionary();

private:
    Ui::LibreDictionary *ui;
};

#endif // LIBREDICTIONARY_H
