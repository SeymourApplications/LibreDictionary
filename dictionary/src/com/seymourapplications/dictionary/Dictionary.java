package com.seymourapplications.dictionary;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dictionary implements Serializable {
	private static final long serialVersionUID = -4373602847941703354L;

	public enum DictType {ZH_EN}
	public enum Language {ZH, EN}

    private ArrayList<Word> Words = new ArrayList<>();

    public static final String WORD = "com.seymourapplications.libredictionary.Dictionary.Word";
    
    public static final String DICTNAME_ZH_EN = "cedict_raw";

    public static class Word implements Serializable {
		private static final long serialVersionUID = 4161945161542740080L;
		
		public String Word;
		public String Pronunciation;
		public Language Lang;

		public String[] Definitions;

        public Word(String word, String pron, Language lang, String[] defs) {
            Word = word;
            Pronunciation = pron;
            Lang = lang;
            Definitions = defs;
        }
    }

    public Word[] getWordsArray() {
        Word[] newArray = new Word[Words.size()];
        newArray = Words.toArray(newArray);

        return newArray;
    }
    
    // This should NEVER be called on android
    public static void GenerateDict(DictType dict) {
    	Dictionary newDict = new Dictionary();
    	String dictName = "empty";
    	
    	switch(dict) {
	        case ZH_EN:
	            try {
	            	dictName = DICTNAME_ZH_EN;
	            	
	                String regex = "(.+) (.+) \\[((?:[a-z]+\\d\\s?)+)\\] (\\/.+\\/)+";
	                Pattern pattern = Pattern.compile(regex);
	                Matcher matcher;
	
	                FileInputStream inputStream = new FileInputStream("cedict_ts.txt");
	                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	                String line;
	                while ((line = reader.readLine()) != null) {
	                    matcher = pattern.matcher(line);
	
	                    if(matcher.find()) {
	                        newDict.Words.add(new Word(matcher.group(2) + " (" + matcher.group(1) + ")",
	                                matcher.group(3), Language.ZH, matcher.group(4).split("/")));
	                    }
	                }
	                
	                reader.close();
	            } catch(Exception e) {
	                e.printStackTrace();
	            }
	            break;
    	}
    	
    	File dir = new File("raw");
    	if(!dir.exists()) dir.mkdir();
    	
 	   	try{
 			FileOutputStream fout = new FileOutputStream("raw/" + dictName);
 			ObjectOutputStream oos = new ObjectOutputStream(fout);   
 			oos.writeObject(newDict);
 			oos.close();
 	   	} catch(Exception ex){
 	   		ex.printStackTrace();
 	   	}
    }
    
    private Dictionary() {}
}






























